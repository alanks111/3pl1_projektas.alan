﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models = Dal.Models;
using MySql.Data.MySqlClient;
using FormModels = Dal.FormModels;
using Dal;

namespace Services
{
    public class BooksService : BaseService, IDisposable
    {
        private readonly LibraryDbContext _db;
        public BooksService()
        {
            _db = new LibraryDbContext();
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        //public List<Book> SearchBooks(BookSearch bookSearch)
        //{
        //    using (var connection = GetConnection())
        //    {
        //        var searchBooksCmdText = "Filter_books";
        //        var searchBooksCmd = new MySqlCommand(searchBooksCmdText, connection);
        //        searchBooksCmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        searchBooksCmd.Parameters.AddWithValue("InIsbn", string.IsNullOrWhiteSpace(bookSearch.Isbn) ? null : bookSearch.Isbn);
        //        searchBooksCmd.Parameters.AddWithValue("InTitle", string.IsNullOrWhiteSpace(bookSearch.Title) ? null : bookSearch.Title);
        //        searchBooksCmd.Parameters.AddWithValue("InAuthor", string.IsNullOrWhiteSpace(bookSearch.Author) ? null : bookSearch.Author);
        //        searchBooksCmd.Parameters.AddWithValue("InCategoryIds", "-1");

        //        MySqlDataAdapter adapter = new MySqlDataAdapter(searchBooksCmd);
        //        DataTable dataTable = new DataTable();
        //        adapter.Fill(dataTable);

        //        var list = dataTable.AsEnumerable().Select(m => new Book
        //        {
        //            BookId = m.Field<int>("Id"),
        //            Isbn = m.Field<string>("Isbn"),
        //            Title = m.Field<string>("Title"),
        //            DateOfPublish = m.Field<DateTime?>("DateOfPublish"),
        //            Categories = m.Field<string>("Categories")
        //        }).ToList();

        //        return list;
        //    }
        //}

        public List<FormModels.BookFilter> FilterBooks()
        {
            var data = (from book in _db.Books
                        let categories = _db.Categories.Join(_db.BooksByCategories,
                                            category => category.Id,
                                            bookByCategory => bookByCategory.CategoryId,
                                            (category, bookByCategory) => new { Category = category, BookByCategory = bookByCategory })
                                            .Where(m => m.BookByCategory.BookId == book.Id)
                                            .Select(m => m.Category.Name)
                                            .ToList()
                        let authors = _db.Authors.Join(_db.BooksByAuthors,
                                            author => author.Id,
                                            bookByAuthor => bookByAuthor.AuthorId,
                                            (author, bookByAuthor) => new { Author = author, BookByAuthor = bookByAuthor })
                                    .Where(m => m.BookByAuthor.BookId == book.Id)
                                    .Select(m => m.Author.Firstname + " " + m.Author.Lastname).ToList()
                        select new
                        {
                            BookId = book.Id,
                            Title = book.Title,
                            Isbn = book.Isbn,
                            DateOfPublish = book.DateOfPublish,
                            Categories = categories,
                            Authors = authors
                        })
                        .AsEnumerable()
                        .Select(m => new FormModels.BookFilter
                        {
                            BookId = m.BookId,
                            Isbn = m.Isbn,
                            Title = m.Title,
                            DateOfPublish = m.DateOfPublish,
                            Categories = string.Join(", ", m.Categories),
                            Authors = string.Join(", ", m.Authors)
                        }).ToList();
            return new List<FormModels.BookFilter>();
        }
    }
}
