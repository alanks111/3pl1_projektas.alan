﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Models;
using MySql.Data.MySqlClient;

namespace Services
{
    public class AuthorService : BaseService, IDisposable
    {
        public AuthorService()
        {

        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        //public List<Author> GetAuthors()
        //{
        //    using (var connection = GetConnection())
        //    {
        //        var cmdText = "Get_Authors";
        //        var command = new MySqlCommand(cmdText, connection);
        //        command.CommandType = System.Data.CommandType.StoredProcedure;
        //        connection.Open();
        //        var list = new List<Author>();

        //        var reader = command.ExecuteReader();

        //        while (reader.Read())
        //        {
        //            var author = new Author(
        //                reader.GetInt32("Id"),
        //                reader.GetString("Firstname"), 
        //                reader.GetString("Lastname"), 
        //                reader.GetString("Nationality")
        //            );

        //            list.Add(author);
        //        }

        //        connection.Close();
        //        return list;
        //    }
        //}
    }
}
