﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Models
{
    public class BookSearch
    {
        public string Isbn { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public List<int> Categories { get; set; }
    }
}
