﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.EntityFramework;
using Dal.Models;

namespace Dal
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class LibraryDbContext : DbContext
    {
        public LibraryDbContext()
            : base("library")
        {

        }

        public DbSet<CategoryModel> Categories { get; set; }

        public DbSet<Book> Books { get; set; }

        public DbSet<BookByAuthor> BooksByAuthors { get; set; }

        public DbSet<BookByCategory> BooksByCategories { get; set; }

        public DbSet<Author> Authors { get; set; }
    }
}
